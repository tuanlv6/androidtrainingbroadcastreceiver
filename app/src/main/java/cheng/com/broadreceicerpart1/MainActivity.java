package cheng.com.broadreceicerpart1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;


public class MainActivity extends AppCompatActivity {

    public static final String MY_ACTION = "com.tincoder.ACTION";
    public static final String MY_TEXT = "com.tincoder.TEXT";
    private LocaleChangeReceiver localeChangeReceiver;

    private BroadcastReceiver myReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String locale = Locale.getDefault().getCountry();
            Toast.makeText(context, "LOCALE CHANGED to " + locale,
                    Toast.LENGTH_LONG).show();
        }
    };



        private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
          if(MY_ACTION.equals(intent.getAction())){
              String text = intent.getStringExtra(MY_TEXT);
              tvReceived.setText(text);
          }
        }
    };

    private Button btnSendBroadcast;
    private TextView tvReceived;

//    private ExampleBroadCastReceiver exampleBroadCastReceiver;


    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(MY_ACTION);
        registerReceiver(mBroadcastReceiver,intentFilter);

    }

    @Override
    protected void onPause() {
        super.onPause();
        localeChangeReceiver = new LocaleChangeReceiver();
        IntentFilter filter = new IntentFilter("android.intent.action.LOCALE_CHANGED");
        registerReceiver(localeChangeReceiver, filter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSendBroadcast = findViewById(R.id.btn_send_broadcast);
        tvReceived = findViewById(R.id.tv_received);

        btnSendBroadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickSendBroadcast();
            }

            private void clickSendBroadcast() {
                Intent intent = new Intent(MY_ACTION);
                intent.putExtra(MY_TEXT,"This is TinCoder Channel");
                sendBroadcast(intent);
            }
        });
//        exampleBroadCastReceiver = new ExampleBroadCastReceiver();
//    }
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
//        registerReceiver(exampleBroadCastReceiver,intentFilter);
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
////        unregisterReceiver(exampleBroadCastReceiver);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        unregisterReceiver(exampleBroadCastReceiver);



    }
}